@extends('layouts.admin')
@section('main')

    <h1>Add Product</h1>
    <div class="container">
        <form action="{{ route('product.store') }}" class="was-validated" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-9">
                    {{-- @method('PUT') --}}
                    <div class="form-group">
                        <label for="uname">Name:</label>
                        <input type="text" class="form-control" id="name" placeholder="Name product..." name="name">
                        @error('name')
                            <small class="help-block">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="uname">Slug:</label>
                        <input type="text" class="form-control" id="slug" placeholder="Slug here..." name="slug">
                        @error('slug')
                        <small class="help-block">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="uname">Description:</label>
                        <textarea name="description" id="content" class="form-control" rows="3"
                            placeholder="Description here..."></textarea>
                        @error('description')
                            <small class="help-block">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="uname">Image list:<button type="button" class="btn btn-danger" data-toggle="modal"
                                data-target="#modal_list">
                                <i class="fa fa-folder-open"></i>
                            </button></label>
                        <input type="hidden" id="image_list" name="image_list">
                        <div class="row" id="show_image_list">

                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="uname">Category:</label>
                        <select name="idCategory" id="idCategory" class="form-control">
                            <option value="">1</option>
                            @foreach ($cats as $cat)
                                <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                            @endforeach
                        </select>
                        @error('category')
                            <small class="help-block">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="uname">Price:</label>
                        <input type="text" class="form-control" id="price" placeholder="Price of product..." name="price">
                        @error('price')
                            <small class="help-block">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="uname">Sale price:</label>
                        <input type="text" class="form-control" id="sale_price" placeholder="Sale price here..."
                            name="sale_price">
                        @error('sale_price')
                            <small class="help-block">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="uname">Image:</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="image" name="image">
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modelId">
                                        <i class="fa fa-folder-open"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <img src="" alt="" id="show_img" style="width:100%">
                        @error('image')
                            <small class="help-block">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="pwd">Status:</label>
                        <div>
                            <input type="radio" checked value="1" id="status" name="status"> <label for="pwd">Public</label>
                            <input type="radio" value="0" id="status" name="status"> <label for="pwd">Private</label>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Add</button>
        </form>
        <!-- Button trigger modal -->
        {{-- <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modelId">
          Launch
        </button> --}}

        <!-- Modal -->
        <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <iframe src="{{ url('file/dialog.php?field_id=image') }}"
                            style="width:100%; height:500px;border:none; overflow-y:auto" frameborder="0"></iframe>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>

        {{-- image list --}}
        <!-- Modal -->
        <div class="modal fade" id="modal_list" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <iframe src="{{ url('file/dialog.php?field_id=image_list') }}"
                            style="width:100%; height:500px;border:none; overflow-y:auto" frameborder="0"></iframe>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop()
@section('css')
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('ad/plugins/summernote/summernote-bs4.min.css') }}">
@stop()

@section('js')
    <!-- Summernote -->
    <script src="{{ asset('ad/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $(function() {
            // Summernote
            $('#content').summernote({
                height: 150,
                placeholder: "Product description..."
            });

            // CodeMirror
        });

        // hide modal
        $('#modelId').on('hide.bs.modal', event => {
            var _link = $('input#image').val();
            var _img = "{{ url('uploads') }}" + '/' + _link;
            alert(_img);
            $('img#show_img').attr('src', _img);

        });
        $('#modal_list').on('hide.bs.modal', event => {
            var _links = $('input#image_list').val();
            var _html = '';

            if (/^[\],:{}\s]*$/.test(_links.replace(/\\["\\\/bfnrtu]/g, '@').replace(
                    /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(
                    /(?:^|:|,)(?:\s*\[)+/g, ''))) {

                var _args = $.parseJSON(_links);
                for (let i = 0; i < _args.length; i++) {
                    let _url = "{{ url('uploads') }}" + '/' + _args[i];
                    _html += ' <div class="col-md-4">';
                    _html += ' <img src="' + _url + '" alt="" style="width:100%">';
                    _html += ' </div>';

                }
            } else {
                let _url = "{{ url('uploads') }}" + '/' + _links;
                _html += ' <div class="col-md-4">';
                _html += ' <img src="' + _url + '" alt="" style="width:100%">';
                _html += ' </div>';
            }
            // var _img = "{{ url('uploads') }}" + '/' + _links;

            // var _args = $.parseJSON(_links);
            // var _html = '';
            // console.log(_args);
            // for(let i = 0; i < _args.length; i++) {
            //     let _url = "{{ url('uploads') }}" + '/' + _args[i];
            //     _html +=' <div class="col-md-4">';
            //     _html += ' <img src="'+_url+'" alt="" style="width:100%">';
            //     _html += ' </div>';

            // }
            $('#show_image_list').html(_html);
            // alert(_img);
            // $('img#show_img').attr('src',_img);

        });
    </script>
<script src="{{ url('ad') }}/js/slug.js"></script>
@stop()

