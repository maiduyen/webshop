@extends('layouts.admin')
@section('main')

    <?php
    $images = json_decode($product->image_list);
    ?>
    <div class="box-body">
        <div class="row">
            <div class="col-md-5">
                <img src="{{ url('uploads') }}/{{ $product->image}}" alt="" style="width:100%">
                @if(is_array($images))
                <div class="row">
                    <br>
                    @foreach($images as $img)
                        <div class="col-md-4">
                            <img src="{{$img}}" alt="" style="width:100%">
                        </div>
                    @endforeach
                </div>
                @endif
            </div>
            <div class="col-md-2">
                <h3>{{ $product->name}}</h3>
            </div>
        </div>
    </div>

@stop();
