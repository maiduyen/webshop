@extends('layouts.admin')
{{-- @section('title', 'Edit Category') --}}
@section('main')

    <h1>Update Category</h1>
    <div class="container">
        <form action="{{ route('category.update', $category->id) }}" class="was-validated" method="POST">
            @csrf
            @method('PUT')
            <input type="hidden" value="{{ $category->id }}" name="id">
            <div class="form-group">
                <label for="uname">Name:</label>
                <input type="text" value="{{ $category->name }}" class="form-control" name="name" id="name"
                    placeholder="Name...">
                @error('name')
                    <small class="help-block">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="pwd">Status:</label>
                <div>
                    <input type="radio" checked value="1" id="status" name="status"> <label for="pwd">Public</label>
                    <input type="radio" value="0" id="status" name="status"> <label for="pwd">Private</label>
                </div>
            </div>
            <div class="form-group">
                <label for="uname">Prioty:</label>
                <input type="number" value="{{ $category->prioty }}" class="form-control" id="prioty"
                    placeholder="Prioty..." name="prioty">
                @error('prioty')
                    <small class="help-block">{{ $message }}</small>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>

@stop();
