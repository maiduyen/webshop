@extends('layouts.admin')
@section('main')

<h1>List Category</h1>
<form class="form-inline" method="get">
    @csrf
    <div class="form-group">
        <input type="text" name="key" value="{{$key}}" class="form-control" placeholder="Enter name..." aria-describedby="helpId">
    </div>
    <button class="btn btn-primary">
        <i class="fas fa-search"></i>
    </button>
</form>
<hr>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Total Product</th>
                <th>Status</th>
                <th>Create Category</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($data as $cat)    
            <tr>
                <td>{{$cat->id}}</td>
                <td>{{$cat->name}}</td>
                <td>{{$cat->product ->count()}}</td>
                <td>
                    @if($cat->status == 0)
                        <span class="badge badge-danger">Private</span>
                    @else
                        <span class="badge badge-success">Private</span>
                    @endif
                </td>
                <td>{{$cat->created_at}}</td>
                {{-- text-right --}}
                <td class="">
                    <a href="{{route('category.edit', $cat->id)}}" class="btn btn-success">
                        <i class="fas fa-edit"></i>
                    </a>
                    <a href="{{route('category.destroy', $cat->id)}}" class="btn btn-danger btndelete">
                        <i class="fas fa-trash"></i>
                    </a>
                </td>
                
            </tr>
        @endforeach    
        </tbody>
    </table>
    <form action="" method="POST" id="form-delete">
        @csrf
        @method('DELETE')
    </form>
    <hr>
    <div class="">
        {{$data -> appends(request()->all())->links()}}
    </div>
    @stop()

    @section('js')

        <script>
            $('.btndelete').click(function(ev){
                ev.preventDefault();
                var _href = $(this).attr('href');

                $('form#form-delete').attr('action', _href);
                if(confirm('Are you sure you want to delete this category')){
                    $('form#form-delete').submit();
                }
            })
        </script>


@stop()