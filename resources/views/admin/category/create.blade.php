@extends('layouts.admin')
@section('main')

<h1>Add Category</h1>
<div class="container">
    <form action="{{route('category.store')}}" class="was-validated" method="POST">
        @csrf
        {{-- @method('PUT') --}}
      <div class="form-group">
        <label for="uname">Name:</label>
        <input type="text" class="form-control" id="name" placeholder="Name..." name="name">
        @error('name')
        <small class="help-block">{{$message}}</small>
        @enderror
      </div>
      <div class="form-group">
        <label for="uname">Slug:</label>
        <input type="text" class="form-control" id="slug" placeholder="Slug here..." name="slug">
        @error('slug')
        <small class="help-block">{{$message}}</small>
        @enderror
      </div>
      <div class="form-group">
        <label for="pwd">Status:</label>
        <div>
            <input type="radio" checked value="1" id="status" name="status"> <label for="pwd">Public</label>
            <input type="radio" value="0" id="status" name="status"> <label for="pwd">Private</label>
        </div>
      </div>
      <div class="form-group">
        <label for="uname">Prioty:</label>
        <input type="text" class="form-control" id="name" placeholder="Prioty..." name="prioty">
        @error('prioty')
          <small class="help-block">{{$message}}</small>
      @enderror
      </div>
      <button type="submit" class="btn btn-primary">Add</button>
    </form>
  </div>

@stop()

@section('js')
  <script src="{{ url('ad') }}/js/slug.js"></script>
@stop()