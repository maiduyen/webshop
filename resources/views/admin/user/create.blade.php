@extends('layouts.admin')
@section('main')

    <h1>Add Account</h1>
    <div class="container">
        <form action="{{ route('user.store') }}" class="was-validated" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    {{-- @method('PUT') --}}
                    <div class="form-group">
                        <label for="uname">Name:</label>
                        <input type="text" class="form-control" id="name" placeholder="Name user..." name="name">
                        @error('name')
                            <small class="help-block">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="uname">Email:</label>
                        <input type="email" class="form-control" id="email" placeholder="Email here..." name="email">
                        @error('email')
                        <small class="help-block">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="uname">Password:</label>
                        <input type="password" class="form-control" id="password" placeholder="Password here..." name="password">
                        @error('password')
                        <small class="help-block">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="uname">Confirm password:</label>
                        <input type="password" class="form-control" id="confirm-password" placeholder="Confirm password here..." name="confirm-password">
                        @error('confirm-password')
                        <small class="help-block">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="uname">Image:</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="profile_photo_path" name="profile_photo_path">
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modelId">
                                        <i class="fa fa-folder-open"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <img src="" alt="" id="show_img" style="width:100%">
                        @error('profile_photo_path')
                            <small class="help-block">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Add</button>
        </form>
        <!-- Button trigger modal -->
        {{-- <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modelId">
          Launch
        </button> --}}

        <!-- Modal -->
        <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <iframe src="{{ url('file/dialog.php?field_id=profile_photo_path') }}"
                            style="width:100%; height:500px;border:none; overflow-y:auto" frameborder="0"></iframe>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop()
@section('css')
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('ad/plugins/summernote/summernote-bs4.min.css') }}">
@stop()

@section('js')
    <!-- Summernote -->
    <script src="{{ asset('ad/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $(function() {
            // Summernote
            $('#content').summernote({
                height: 150,
                placeholder: "Product description..."
            });

            // CodeMirror
        });

        // hide modal
        $('#modelId').on('hide.bs.modal', event => {
            var _link = $('input#profile_photo_path').val();
            var _img = "{{ url('uploads') }}" + '/' + _link;
            alert(_img);
            $('img#show_img').attr('src', _img);

        });
    </script>
<script src="{{ url('ad') }}/js/slug.js"></script>
@stop()

