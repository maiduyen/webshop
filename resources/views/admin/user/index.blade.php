@extends('layouts.admin')
@section('main')

<h1>List Account</h1>
<form action="" class="form-inline" method="get">
    @csrf
    <div class="form-group">
        <input class="form-control" name="key" value="{{$key}}" class="form-control" placeholder="Enter name..." aria-describedby="helpId">
    </div>
    <button type="submit" class="btn btn-primary">
        <i class="fas fa-search"></i>
    </button>
</form>
<hr>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                {{-- <th>Password</th> --}}
                {{-- <th>Image</th> --}}
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($data as $model)    
            <tr>
                <td>{{$model->id}}</td>
                <td>{{$model->name}}</td>
                <td>{{$model->email}}</td>
                {{-- <td>{{$model->password}}</td> --}}
                {{-- <td>
                    
                    <img src="{{url('uploads')}}/{{$model->profile_photo_path}}" width="60" alt="">
                </td> --}}
                {{-- text-right --}}
                <td class="">
                    <a href="{{route('user.edit', $model->id)}}" class="btn btn-success">
                        <i class="fas fa-edit"></i>
                    </a>
                    <a href="{{route('user.destroy', $model->id)}}" class="btn btn-danger btndelete">
                        <i class="fas fa-trash"></i>
                    </a>
                </td>
                
            </tr>
        @endforeach    
        </tbody>
    </table>
    <form action="" method="POST" id="form-delete">
        @csrf
        @method('DELETE')
    </form>
    <hr>
    <div class="">
        {{$data -> appends(request()->all())->links()}}
    </div>
    @stop()

    @section('js')

        <script>
            $('.btndelete').click(function(ev){
                ev.preventDefault();
                var _href = $(this).attr('href');

                $('form#form-delete').attr('action', _href);
                if(confirm('Are you sure you want to delete this category')){
                    $('form#form-delete').submit();
                }
            })
        </script>


@stop()