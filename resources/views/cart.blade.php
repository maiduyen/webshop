@extends('layouts.site')

@section('main')


<title>Cart</title>

<!-- Css Styles -->
<link rel="stylesheet" href="{{ asset('site/css/bootstrap.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('site/css/font-awesome.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('site/css/elegant-icons.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('site/css/nice-select.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('site/css/jquery-ui.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('site/css/owl.carousel.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('site/css/slicknav.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('site/css/style.css') }}" type="text/css">
<!-- Breadcrumb Section Begin -->
<section class="breadcrumb-section set-bg" data-setbg="img/breadcrumb.jpg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2>Shopping Cart</h2>
                    <div class="breadcrumb__option">
                        <a href="./index.html">Home</a>
                        <span>Shopping Cart</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Section End -->

<!-- Shoping Cart Section Begin -->
<section class="shoping-cart spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="shoping__cart__table">
                    <table>
                        <thead>
                            <tr>
                                <th class="shoping__product">Products</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($cart->items as $item)
                            <tr>

                                <td class="shoping__cart__item">
                                    {{-- {{url('uploads')}}/{{ $item['image'] }} --}}
                                    <img src="{{url('uploads')}}/{{ $item['image'] }}" class="img-responsive" alt="" style="width:40%; height:200px">
                                    <h5>{{ $item['name'] }}</h5>
                                </td>
                                <td class="shoping__cart__price">
                                    {{ number_format($item['price']) }}đ
                                </td>
                                <td class="shoping__cart__quantity">
                                    <form action="{{ route('cart.update', [$item['id']]) }}" method="GET">
                                        <div class="quantity">
                                            <div class="pro-qty">
                                                <input type="text" name="quantity" style="width:10%" value="{{ $item['quantity'] }}">
                                            </div>
                                        </div>
                                        <input type="submit" value="Update" class="btn btn-success">
                                    </form>
                                </td>
                                <td class="shoping__cart__total">
                                    {{ number_format($item['price']*$item['quantity']) }}đ
                                </td>
                                <td class="shoping__cart__item__close">
                                    {{-- <span class="icon_close"> --}}
                                        <a href="{{ route('cart.remove', [$item['id']]) }}"><span class="icon_close"> </span></a>
                                    {{-- </span> --}}
                                </td>
                            </tr>
                            @endforeach
                               
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            {{-- <div class="col-lg-12"> --}}
                <div class="col-lg-6">
                    <div class="shoping__cart__btns">
                        <a href="{{ route('home.index') }}" target="_blank" class="primary-btn cart-btn">CONTINUE SHOPPING</a>
                        
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="shoping__cart__btns text-right">
                        <a href="{{ route('cart.clear') }}" onclick="return confirm('Are you sure delete all cart')" class="btn btn-danger">Remove All</a>
                    </div>
                </div>
            {{-- </div> --}}
            <div class="col-lg-6">
                {{-- <div class="shoping__continue">
                    <div class="shoping__discount">
                        <h5>Discount Codes</h5>
                        <form action="#">
                            <input type="text" placeholder="Enter your coupon code">
                            <button type="submit" class="site-btn">APPLY COUPON</button>
                        </form>
                    </div>
                </div> --}}
            </div>
            <div class="col-lg-6">
                <div class="shoping__checkout">
                    <h5>Cart Total</h5>
                    <ul>
                        {{-- <li>Subtotal <span>$454.98</span></li> --}}
                        <li>Total <span>{{ number_format($cart->total_price) }}đ</span></li>
                    </ul>
                    <a href="#" class="primary-btn">PROCEED TO CHECKOUT</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Shoping Cart Section End -->

    <!-- Product Section End -->
    <script src="{{ asset('site/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('site/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('site/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('site/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('site/js/jquery.slicknav.js') }}"></script>
    <script src="{{ asset('site/js/mixitup.min.js') }}"></script>
    <script src="{{ asset('site/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('site/js/main.js') }}"></script>
@stop();