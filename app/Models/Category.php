<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $table = 'category';
    public $primaryKey = 'id';
    // public $timestamps = false;
    protected $fillable = [
        'name', 'slug', 'status', 'prioty'
    ];
// join 1-n
    public function product(){
        return $this->hasMany(Product::class,'idCategory', 'id');
    }

    // global scope
    public function scopeSearch($query){
        if($key = request()->key){
            $query = $query->where('name', 'like', "%$key%");
        }
        return $query;    
    }
}
