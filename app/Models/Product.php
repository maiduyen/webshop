<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'product';
    public $primaryKey = 'id';
    // public $timestamps = false;
    protected $fillable = [
        'name', 'slug', 'image', 'price', 'sale_price', 'description', 'image_list', 'status', 'idCategory'
    ];

    public function cat(){
        return $this->hasOne(Category::class,'id', 'idCategory');
    }

    public function details(){
        return $this->hasMany(OrderDetail::class,'idProduct', 'id');
    }

    // global scope
    public function scopeSearch($query){
        if($key = request()->key){
            $query = $query->where('name', 'like', "%$key%");
        }
        return $query;    
    }
}
