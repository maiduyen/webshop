<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required|unique:product',
            'description' => 'required',
            'image_list' => 'required',
            'idCategory' => 'required',
            'price' => 'required|numeric|min:0|not_in:0',
            'sale_price' => 'required|numeric|min:0|lt:price',
            'status' => 'required',
            'image' => 'required'
        ];
    }
    public function messages()
    {
        return [
            //
            'name.required' => 'Product name can not be blank',
            'description' => 'Description can not be blank',
            'image_list' => 'Please choose image ',
            'idCategory' => 'Please choose Category',
            'price.required' => 'Price of product can not be blank',
            'status.required' => 'Status can not be blank',
            'image.required' => 'Please choose image',
            'name.unique' => 'This product is available in Database',
            'sale_price.lt' => 'Sale price < Price',
            'price.not_in' => 'Price > 0'
        ];
    }
}
