<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required|unique:product,name,' .request()->id,
            // 'description' => 'required',
            'image_list' => 'required',
            'idCategory' => 'required',
            'price' => 'required',
            'status' => 'required',
            'image' => 'required'
        ];
    }
    public function messages()
    {
        return [
            //
            'name.required' => 'Product name can not be blank',
            // 'description' => 'Description can not be blank',
            'image_list' => 'Please choose image ',
            'idCategory' => 'Please choose Category',
            'price.required' => 'Price of product can not be blank',
            'status.required' => 'Status can not be blank',
            'image.required' => 'Please choose image',
            'name.unique' => 'This product is available in Database',
        
        ];
    }
}
