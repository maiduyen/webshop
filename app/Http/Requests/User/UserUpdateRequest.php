<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required|unique:user,name,' .request()->id,
            
            // 'description' => 'required',
            'email'     => 'required|string|email|max:255',
            'password'  => 'required|string|min:8|confirmed',
        ];
    }
    public function messages()
    {
        return [
            //
            'name.required' => 'Name user name can not be blank',
            'email.required' => 'Email name can not be blank',
            'password.required' => 'Password can not be blank',
            'name.unique' => 'This product is available in Database',
            'email.unique' => 'This e-mail is already taken',
        ];
    }
}
