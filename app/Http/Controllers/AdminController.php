<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Account\LoginRequest;
use Illuminate\Support\Facades\Auth ;
// use Auth;

class AdminController extends Controller
{
    //
    public function dashboard()
    {
        //
        return view('admin.dashboard');
    }
    public function file()
    {
        //
        
        return view('admin.file');
    }
    public function index()
    {
        //
        // echo 'Helo world';
        return view('admin.account.index');
    }



    
    public function login()
    {
        //
        // echo 'login';
        return view('admin.account.login');
    }
    public function post_login(LoginRequest $request)
    {
        // $request->has('remember')
        if(Auth::attempt($request->only('email','password'),$request->has('remember'))){

            $name = Auth::user()->name;
            // dd($name);

            return redirect()->route('admin.dashboard');

            // echo 'Success login';
        }else{
            return redirect()->back();
            // echo 'False login';
        }
    }
    public function logout()
    {
        //
        // echo 'login';
        Auth::logout();

        return redirect()->route('login');
    }
    }
// }

