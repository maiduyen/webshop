<?php

namespace App\Http\Controllers;
use App\Helper\CartHelper;

use Illuminate\Http\Request;
use App\Models\Product;

class CartController extends Controller
{
    //
    public function view(){
        return view('cart');
    }
    public function add(CartHelper $cart, $id){
        $quantity = request()->quantity ? request()->quantity : 1;
        $product = Product::find($id);
        $cart->add($product, $quantity);
        // dd(session('cart'));
        return redirect()->back();
    }

    public function remove(CartHelper $cart, $id){
        // $product = Product::find($id);
        $cart->remove($id);
        // dd(session('cart'));
        return redirect()->back();
    }

    public function update(CartHelper $cart, $id){
        $quantity = request()->quantity ? request()->quantity : 1;
        $cart->update($id, $quantity);
        // dd(session('cart'));
        return redirect()->back();
    }

    public function clear(CartHelper $cart){
        
        $cart->clear();
        // dd(session('cart'));
        return redirect()->back();
    }
}
