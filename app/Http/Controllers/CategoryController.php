<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Requests\Category\StoreRequest;
use App\Http\Requests\Category\UpdateRequest;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $key = $request->get('key');
        $data = Category::orderBy('created_at', 'DESC')->search()->paginate(3);
        return view('admin.category.index', compact('data', 'key'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        //
        // $request->validate([
        //     'name' => 'required|unique:category',
        //     'prioty' => 'required'
        // ],[
        //     'name.required' => 'Tên danh mục không để trống',
        //     'prioty.required' => 'Thứ tự ưu tiên không để trống',
        //     'name.unique' => 'Danh mục này đã có trong CSDL',
        // ]);
        if(Category::create($request->all())){
            return redirect()->route('category.index')->with('success','Add new category success!');
        }
        // $category = new Category();
        // $category -> name = $request-> get('name');
        // $category -> status = $request->get('status');
        // $category -> prioty = $request->get('prioty');
        // $category -> save();
        // return redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
        return view('admin.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Category $category)
    {
        //
        // $request->validate([
        //     'name' => 'required|unique:category,name,' .$category->id,
        //     'prioty' => 'required'
        // ],[
        //     'name.required' => 'Tên danh mục không để trống',
        //     'prioty.required' => 'Thứ tự ưu tiên không để trống',
        //     'name.unique' => 'Danh mục này đã có trong CSDL',
        // ]);

        $category->update($request->only('name','status','prioty'));
        return redirect()->route('category.index')->with('update','Update Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
        if($category->product->count() > 0){
            return redirect()->route('category.index')->with('error', 'Danh mục đang còn sản phẩm');
        }else{
            $category->delete();
            return redirect()->route('category.index')->with('success','Xóa thành công!');
        }
        
    }
}
