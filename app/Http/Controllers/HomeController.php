<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Models\Product;
Use App\Models\Category;

// Use App\Http\Controllers\ProductController;

class HomeController extends Controller
{
    //
    public function index()
    {
        //
        $category = Category::where('status', 1)->orderBy('name', 'ASC')->get();
        $top_product = Product::limit(8)->orderBy('id', 'DESC')->get();
        $sale_product = Product::where('sale_price', '>', 0)->limit(8)->orderBy('id', 'DESC')->get();
        
        return view('home', compact('category', 'top_product', 'sale_product'));

    }

    public function shop()
    {
        //
        $category = Category::where('status', 1)->orderBy('name', 'ASC')->get();
        $sale_off = Product::orderBy('name', 'ASC')->where('status', 1)->limit(6)->get();
        $product_list = Product::orderBy('name', 'ASC')->where('status', 1)->limit(9)->paginate(9);
        $new_product=Product::inRandomOrder()->where('sale_price', '>', 0)->limit(3)->get();
        return view('shop', compact('category', 'sale_off', 'product_list', 'new_product'));

    }

    //contact
    public function contact()
    {
        //
        return view('contact');
    }
    public function about()
    {
        //
        return view('about');
    }
    // public function view($slug)
    // {
    //     //
    //     $model = Category::where('slug', $slug)->first();
    //     $category = Category::where('status', 1)->orderBy('name', 'ASC')->get();
    //     return view('product', ['model' => $model, 'category' => $category ]);
    // }

    public function showCategory($slug)
    {
        //
        // $article = Category::where('slug', $slug)->firstOrFail();
        $category = Category::where('status', 1)->orderBy('name', 'ASC')->get();
        $model = Category::where('slug', $slug)->first();
        $product = Product::where('slug', $slug)->first();
        $product_list = Product::orderBy('name', 'ASC')->where('status', 1)->limit(9)->paginate(9);
        $new_product=Product::inRandomOrder()->where('sale_price', '>', 0)->limit(3)->get();
        $relate_product=Product::inRandomOrder()->where('sale_price', '>', 0)->limit(4)->get();
        if($model){
            return view('showCategory', compact('category', 'model', 'product', 'product_list', 'new_product', 'relate_product'));
        }else if($product){
            return view('product-detail', compact('category', 'product', 'product_list', 'new_product', 'relate_product'));
        }else{
            return view('errors/404');
        }
        // $sale_off = Product::orderBy('name', 'ASC')->where('status', 1)->limit(6)->get();
        
        
    }
}
