<?php

namespace App\Http\Controllers;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Requests\Product\ProductStoreRequest;
use App\Http\Requests\Product\ProductUpdateRequest;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $key = $request->get('key');
        $data = Product::orderBy('created_at', 'DESC')->search()->paginate(3);
        return view('admin.product.index', compact('data', 'key'));
   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $cats = Category::orderBy('name', 'ASC')->select('id', 'name')->get();
        return view('admin.product.create', compact('cats'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductStoreRequest $request)
    {
        //bo upload anh vi da cho select
        // if($request->has('file_upload')){
        //     $file = $request -> file_upload;
        //     $ext = $request->file_upload->extension();
        //     $file_name = time().'-'.'product.'.$ext ;
        //     $file->move(public_path('uploads'), $file_name);

        // }

        // $request->merge(['image' => $file_name]);
        if(Product::create($request->all())){
            return redirect()->route('product.index')->with('success','Thêm mới sản phẩm thành công!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $product = Product::find($id);
        return view('admin/product/show', [
            'product' => $product
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $cats = Category::all();
        return view('admin.product.edit',[
            "product" => $product,
            "cats" => $cats
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUpdateRequest $request, Product $product)
    {
        //
        $product->update($request->only('name', 'description', 'image_list', 'idCategory', 'price','status','image'));
        return redirect()->route('product.index')->with('update','Update product Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
        if($product->details->count() > 0){
            return redirect()->route('product.index')->with('error', 'Không thể xóa sản phẩm này');
        }else{
            $product->delete();
            return redirect()->route('product.index')->with('success','Xóa thành công sản phẩm!');
        }
    }
}
