<?php 
    namespace App\Helper;

    class CartHelper {

        public $item = [];
        public $total_quantity = 0;
        public $total_price = 0;
        public function __construct(){
            $this->items = session('cart') ? session('cart') : [];
            $this->total_price = $this->get_total_price();
            $this->total_quantity = $this->get_total_quantity();
        }

        public function add($product, $quantity){
            // dd($product);
            $item = [
                'id' => $product->id,
                'name' => $product->name,
                'image' => $product->image,
                'price' => $product->sale_price ? $product->sale_price: $product->price,
                'quantity' => $quantity,
            ];
            if(isset($this->items[$product->id])){
                $this->items[$product->id]['quantity'] += $quantity;
            }else{
                $this->items[$product->id] = $item;
            }
            // update sau khi action
            session(['cart' => $this->items]);
        }

        // xoa 1 product
        public function remove($id){
            if(isset($this->items[$id])){
                unset($this->items[$id]);
            }
            session(['cart' => $this->items]);
        }

        public function update($id, $quantity){
            if(isset($this->items[$id])){
                $this->items[$id]['quantity'] = $quantity;
            }
            session(['cart' => $this->items]);
        }

        // xoa het
        public function clear(){
            
            session(['cart' => '']);
        }

        private function get_total_price(){
            $t = 0;
            foreach ($this->items as $item){
                $t += $item['price']*$item['quantity'];
            }
            return $t;
        }

        private function get_total_quantity(){
            $t = 0;
            foreach ($this->items as $item){
                $t += $item['quantity'];
            }
            return $t;
        }
    }
?>