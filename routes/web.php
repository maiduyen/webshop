<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



 /*
 GET => account.index
 GET => account.create - form create
 POST => account.store - when submit form create
 GET => account.show - detail
 GET => account.edit - form update
 PUT => account.update - when submit
 DELETE => account.destroy - when delete


*/
Route::get('/', 'HomeController@index')->name('home.index');
Route::get('/about', 'HomeController@about')->name('home.about');
Route::get('/shop', 'HomeController@shop')->name('home.shop');
Route::get('/{slug}', 'HomeController@showCategory')->name('home.showCategory');
Route::get('/contact', 'HomeController@contact')->name('home.contact');

Route::group(['prefix'=>'cart'], function(){
    Route::get('view', 'CartController@view')->name('cart.view');
    Route::get('add/{id}', 'CartController@add')->name('cart.add');
    Route::get('remove/{id}', 'CartController@remove')->name('cart.remove');
    Route::get('update/{id}', 'CartController@update')->name('cart.update');
    Route::get('clear', 'CartController@clear')->name('cart.clear');
});
// , 'middleware'=>'auth'
Route::group(['prefix'=>'admin', 'middleware'=>'auth'], function(){
    Route::get('/', 'AdminController@dashboard')->name('admin.dashboard');
    Route::get('/file', 'AdminController@file')->name('admin.file');
    Route::get('/logout', 'AdminController@logout')->name('logout');

    Route::resources([
        'category' => 'CategoryController',
        'product' => 'ProductController',
        'banner' => 'BannerController',
        'account' => 'AccountController',
        'blog' => 'BlogController',
        'order' => 'OrderController',
        'user' => 'UserController',
    ]);

});
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
// Route::get('/{slug}', 'HomeController@view')->name('view');

Route::get('admin/login', 'AdminController@login')->name('login');
Route::post('admin/login', 'AdminController@post_login')->name('login');