<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Product extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('product', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',50);
            $table->string('slug',100);
            $table->string('image',200);
            $table->float('price');
            $table->float('sale_price')->default('0');
            $table->text('description', 255);
            $table->text('image_list', 255);
            $table->tinyInteger('status')->default('1');
            $table->unsignedInteger('idCategory');
            $table->foreign('idCategory')->references('id')->on('category');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('product');
    }
}
