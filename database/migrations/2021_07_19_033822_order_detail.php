<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrderDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('orderDetail', function (Blueprint $table) {
            $table->unsignedInteger('idOrder');
            $table->unsignedInteger('idProduct');
            $table->integer('quantity');
            $table->float('price');
            $table->foreign('idOrder')->references('id')->on('order');
            $table->foreign('idProduct')->references('id')->on('product');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('orderDetail');
    }
}
