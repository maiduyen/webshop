<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Blog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('blog', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',50);
            $table->string('slug',100);
            $table->string('image',200);
            $table->string('sumary', 255);
            $table->text('description');
            $table->tinyInteger('status')->default('1');
            $table->unsignedInteger('idAccount');
            $table->foreign('idAccount')->references('id')->on('account');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('blog');
    }
}
